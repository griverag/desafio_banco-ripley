import {AccountType} from '../enums/account-type.enum';

export class ReceiverDto {
  public id?: number;
  public name_dest: string;
  public rut_dest: string;
  public email_dest: string;
  public phone_dest: string;
  public bank_dest: string;
  public account_number_dest: number;
  public type: AccountType;

  constructor(data?: any) {
    this.id = data.id;
    this.name_dest = data.name_dest;
    this.rut_dest = data.rut_dest;
    this.email_dest = data.email_dest;
    this.phone_dest = data.phone_dest;
    this.bank_dest = data.bank_dest;
    this.account_number_dest = data.account_number_dest;
    this.type = data.type;
  }
}
