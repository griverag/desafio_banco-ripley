

export class BanksDto {
  public id?: string;
  public bank: string;

  constructor(data?: any) {
    this.id = data.id;
    this.bank = data.bank;
  }
}
