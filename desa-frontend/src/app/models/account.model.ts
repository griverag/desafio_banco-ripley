import { AccountType } from '../enums/account-type.enum';

export class Account {
  public id?: number;
  public acc_number_cust: number;
  public balance_cust: number;
  public rut_cust: string;
  public type: AccountType;

  constructor(data?: any) {
    this.id = data.id;
    this.acc_number_cust = data.acc_number_cust;
    this.balance_cust = data.balance_cust;
    this.rut_cust = data.rut_cust;
    this.type = data.type;
  }
}
