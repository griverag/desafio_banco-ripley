import { TransactionType } from '../enums/transaction-type.enum';

export class Transaction {
  public id?: number;
  public amount!: number;
  public type!: TransactionType;
  public rut_dest!: string;
  public rut_customer!: string;
  public createdAt?: Date;
  public account!: number;

  constructor(data?: any) {
    if(data){
      this.id = data?.id;
      this.amount = data.amount;
      this.type = data.type;
      this.rut_dest = data.rut_dest;
      this.rut_customer = data.rut_customer;
      this.createdAt = data.createdAt;

    }
  }

  public toJSON() {
    return {
      ...this,
      account: 1,
    }
  }
}
