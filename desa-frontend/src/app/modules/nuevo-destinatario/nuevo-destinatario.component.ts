import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {map, timeout} from 'rxjs/operators';
import {FormBuilder, FormControl , FormGroup, Validators} from '@angular/forms';
import {AccountService} from '../../services/account/account.service';
import {DialogService} from '../../services/dialog/dialog.service';
import {BanksDto} from '../../dtos/banks-dto';

@Component({
  selector: 'app-nuevo-destinatario',
  templateUrl: './nuevo-destinatario.component.html',
  styleUrls: [`./nuevo-destinatario.component.css`]
})
export class NuevoDestinatarioComponent implements OnInit {
  constructor(private http: HttpClient, private formBuilder: FormBuilder,
              private accountService: AccountService,
              private dialog: DialogService) {
    this.formNuevoDestinatario = this.formBuilder.group({});
  }
  public banksData: BanksDto[] = []
  public isLoading = false;
  public formNuevoDestinatario;

  public async ngOnInit(): Promise<void> {
    this.formNuevoDestinatario = this.formBuilder.group({
      rut_dest: [null, Validators.required],
      name_dest: [null, Validators.required],
      email_dest: [null],
      phone_dest: [null],
      bank_dest: [null, Validators.required],
      account_number_dest: [null, Validators.required],
      type: [null,Validators.required],
    });
    this.banksData = await this.loadBanks();
  }
  public async loadBanks(): Promise<BanksDto[]> {
    this.isLoading = true;
    return new Promise((resolve, rejects) => {
      this.accountService.getBanks().subscribe(response => {
        if (!response.length) this.dialog.danger('Bank Service No Respond - empty')
        this.isLoading = false;
        resolve(response.map(banks => new BanksDto(banks)));
      }, error => {
        this.dialog.danger('Bank Service not responding');
        this.isLoading = false;
      });
    });
  }
  public formatRut(): void {
    let rut = this.formNuevoDestinatario.get('rut_dest')!.value.toString();
    rut = rut.replace('.', '').replace('-', '');
    const module = rut.substr(rut.length - 1);
    rut = rut.slice(0, -1);
    rut = rut.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    rut = rut + `-${module}`;
    this.formNuevoDestinatario.get('rut_dest')!.setValue(rut);
  }

  public async submitForm(): Promise<void> {
    this.isLoading = true;
    const makeReceiverAccount = this.formNuevoDestinatario.getRawValue();
        await this.accountService.makeReceiver(makeReceiverAccount).toPromise();
    this.dialog.success('Destinatario Creado.');
    this.isLoading = false;
  }
}
