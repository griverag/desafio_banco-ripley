import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { IconsProviderModule } from 'src/app/icons-provider.module';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { NuevoDestinatarioComponent } from '../nuevo-destinatario/nuevo-destinatario.component';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzSpinModule} from 'ng-zorro-antd/spin';
import { NzButtonModule } from 'ng-zorro-antd/button';
import {HistorialComponent} from '../historial/historial.component';
import {NzTableModule} from 'ng-zorro-antd/table';
import { NzTagModule } from 'ng-zorro-antd/tag';
import {PipesModule} from '../../pipes/pipes.module';
import {DialogService} from '../../services/dialog/dialog.service';
import {NzMessageModule} from 'ng-zorro-antd/message';
import {TransferirComponent} from '../transferir/transferir.component';

@NgModule({
  imports: [
    HomeRoutingModule,
    IconsProviderModule,
    NzLayoutModule,
    NzMenuModule,
    FormsModule,
    CommonModule,
    NzToolTipModule,
    NzBreadCrumbModule,
    NzFormModule,
    NzSelectModule,
    NzSpinModule,
    ReactiveFormsModule,
    NzButtonModule,
    NzTableModule,
    NzTagModule,
    PipesModule,
    NzMessageModule,
  ],
  declarations: [HomeComponent, NuevoDestinatarioComponent, HistorialComponent,TransferirComponent],
  exports: [HomeComponent],
  providers: [DialogService],
})
export class HomeModule { }
