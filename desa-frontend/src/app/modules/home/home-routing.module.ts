import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NuevoDestinatarioComponent} from '../nuevo-destinatario/nuevo-destinatario.component';
import {HistorialComponent} from '../historial/historial.component';
import {TransferirComponent} from '../transferir/transferir.component';

const routes: Routes = [{
  path: '',
  redirectTo: 'nuevo-destinatario',
  pathMatch: 'full'
}, {
  path: 'nuevo-destinatario',
  component: NuevoDestinatarioComponent
}
, {
    path: 'historial-transferencias',
    component: HistorialComponent
  },
  {
    path: 'transferencias',
    component: TransferirComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
