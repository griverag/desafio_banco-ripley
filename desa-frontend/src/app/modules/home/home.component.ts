import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public isCollapsed = false;
  public items = [];

  constructor(
    private router: Router
  ) { }

  public ngOnInit(): void {

  }

  public change(element: any): void {
    this.router.navigate([element]);
  }

  public getRoute(): string {
    return this.router.url;
  }

  public logOut(): void {
    localStorage.clear();
    window.location.reload();
  }
}
