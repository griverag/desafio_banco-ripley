import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import {AccountService} from '../../services/account/account.service';
import {Transaction} from '../../models/transaction.model';
import {DialogService} from '../../services/dialog/dialog.service';
import * as dayjs from 'dayjs';
import {TransactionType} from '../../enums/transaction-type.enum';

interface Banks {
  name: string;
  id: string;
}

@Component({
  selector: 'app-historial',
  templateUrl: './historial.component.html',
  styleUrls: [`./historial.component.css`]
})
export class HistorialComponent implements OnInit {
  public transactions: Transaction[] = [];
  public transactionType = TransactionType;
  public isLoading: boolean;
  public currentDate: string;

  constructor(private http: HttpClient, private accountService: AccountService,
              private dialog: DialogService)
  {
    this.isLoading = false;
    this.currentDate = '';

  }
  ngOnInit(): void {
    this.getTransferHistory();
    this.getCurrentDate();
  }
  public getTransferHistory(): void {
    this.isLoading = true;
    this.accountService.getHistory().subscribe(response => {
      this.transactions = response.map(transaction => new Transaction(transaction));
      this.isLoading = false;
    }, error => {
      this.isLoading = false;
      this.dialog.danger('Error al obtener historial de transacciones');
    });
  }
  public getCurrentDate(): void {
    this.currentDate = dayjs().format('DD/MM/YYYY HH:mm');
  }
}
