import {Component, OnInit} from '@angular/core';
import {BanksDto} from '../../dtos/banks-dto';
import {Transaction} from '../../models/transaction.model';
import {AccountService} from '../../services/account/account.service';
import {HttpClient} from '@angular/common/http';
import {DialogService} from '../../services/dialog/dialog.service';
import {Receiver_account} from '../../models/receiver_account.model';
import {ReceiverDto} from '../../dtos/receiver-dto';
import {AccountType} from '../../enums/account-type.enum';
import {TransactionType} from '../../enums/transaction-type.enum';

@Component({
  selector: 'app-transferir',
  templateUrl: './transferir.component.html',
  styleUrls: ['./transferir.component.css']
})
export class TransferirComponent implements OnInit {

  constructor(private accountService: AccountService,
              private http: HttpClient,
              private dialog: DialogService) {}

  public monto!: number;
  public receivers: ReceiverDto[] = [];
  public banks: BanksDto[] = [];
  public isLoading = false;
  public receiver!: ReceiverDto;

  public async loadReceiver(): Promise<Receiver_account[]> {
    this.isLoading = true;
    return new Promise((resolve, rejects) => {
      this.accountService.getReceiver().subscribe(response => {
        if (!response.length) this.dialog.danger('Bank Service No Respond - empty')
        this.isLoading = false;
        resolve(response.map(receiver => new Receiver_account(receiver)));
      }, error => {
        this.dialog.danger('Bank Service not responding');
        this.isLoading = false;
      });
    });
  }

  public async loadBanks(): Promise<BanksDto[]> {
    this.isLoading = true;
    return new Promise((resolve, rejects) => {
      this.accountService.getBanks().subscribe(response => {
        if (!response.length) this.dialog.danger('Bank Service No Respond - empty')
        this.isLoading = false;
        resolve(response.map(banks => new BanksDto(banks)));
      }, error => {
        this.dialog.danger('Bank Service not responding');
        this.isLoading = false;
      });
    });
  }

  public getBank(receiver: ReceiverDto): BanksDto | undefined {
    return this.banks.find(bank => bank.id === receiver?.bank_dest);
  }

  public getAccountType(receiver: ReceiverDto): string {
    switch (receiver?.type) {
      case AccountType.CORRIENTE:
        return "Cuenta Corriente";
      case AccountType.CHEQUERA_ELECTRONICA:
        return "Chequera electronica";
      case AccountType.VISTA:
        return "Cuenta Vista";
      case AccountType.CUENTA_RUT:
        return "Cuenta Rut";
      default:
        return "No Identificada";
    }
  }

  public async transfer() {
    if(this.monto >0) {
      const transaction = new Transaction();
      transaction.amount = this.monto;
      transaction.rut_dest = this.receiver.rut_dest;
      transaction.type = TransactionType.TRANSFEROUT;
      transaction.rut_customer = '1-9';
      const finalizada = await this.accountService.transferFounds(transaction.toJSON()).toPromise();
      if (finalizada) {
        this.dialog.success("Transferencia Realizada!!!")
      } else {
        this.dialog.danger("No puedo Realizar la Transferencia.")
      }
    } else {
      this.monto=0;
      this.dialog.danger("Monto:"+this.monto+" Es Inferior a 0")
    }
  }
  public async ngOnInit(): Promise<void> {
    this.receivers = await this.loadReceiver();
    this.banks = await this.loadBanks()
  }
}
