import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RequestTarget } from 'src/app/enums/request-target.enum';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RestService {
   private token: string;
   private rut_customer: string;

  constructor(private http: HttpClient) {
    this.token = '';
    this.rut_customer = '';
  }

  public get<T>(uri: string, options: any = {}): Observable<T> {
    const option = this.generateDefault(options);

    return this.http.get(`${this.getPath(option.target)}/${uri}`, this.generateOptions(option)) as Observable<T>;
  }
  public getExternal<T>(uri: string, options: any = {}): Observable<T> {
    const option = this.generateDefault(options);

    return this.http.get(`${uri}`, this.generateOptions(option)) as Observable<T>;
  }


  public post<T>(uri: string, data: any, options: any = {}): Observable<T> {
    const option = this.generateDefault(options);
    return this.http.post(`${this.getPath(option.target)}/${uri}`, data, this.generateOptions(option)) as Observable<T>;
  }

  public patch<T>(uri: string, data: any, options: any = {}): Observable<T> {
    const option = this.generateDefault(options);

    return this.http.patch(`${this.getPath(option.target)}/${uri}`, data, this.generateOptions(option)) as Observable<T>;
  }

  public put<T>(uri: string, data: any, options: any = {}): Observable<T> {
    const option = this.generateDefault(options);

    return this.http.put(`${this.getPath(option.target)}/${uri}`, data, this.generateOptions(option)) as Observable<T>;
  }

  public delete<T>(uri: string, options: any = {}): Observable<T> {
    const option = this.generateDefault(options);

    return this.http.patch(`${this.getPath(option.target)}/${uri}`, this.generateOptions(option)) as Observable<T>;
  }

  public createHeaders(auth: 1 = 1): HttpHeaders {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=UTF-8');
    headers = headers.set('Access-Control-Allow-Origin', '*');
    headers = headers.set('Access-Control-Allow-Headers', '*');
    // headers = headers.set('rut_customer', `${this.rut_customer}`);
    headers = headers.set('rut_customer', `1-9`);
    return headers;
  }

  private generateDefault(options: any = {}): any {
    return {
      target: RequestTarget.PLATFORM,
      auth: 1,
      ...options
    };
  }

  private generateOptions(options: any = {}): { headers: HttpHeaders} {
    const opt = this.generateDefault(options);
    return {
      headers: this.createHeaders(opt.auth)
    };
  }

  public getPath(target: RequestTarget): string {
    switch (target) {
      case RequestTarget.AUTHENTICATION:
        return environment.API_AUTHENTICATION;
      case RequestTarget.PLATFORM:
        return environment.API_PLATFORM;
      default:
        return environment.API_PLATFORM;
    }
  }
}
