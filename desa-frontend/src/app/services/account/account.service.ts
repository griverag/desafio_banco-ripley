import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Transaction } from 'src/app/models/transaction.model';
import { RestService } from '../rest/rest.service';
import { Account } from 'src/app/models/account.model';
import { TransactionDto } from 'src/app/dtos/transaction.dto';
import {Receiver_account } from 'src/app/models/receiver_account.model';
import {ReceiverDto} from '../../dtos/receiver-dto';
import {BanksDto} from '../../dtos/banks-dto';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(
    private rest: RestService,
    private http: HttpClient,
  ) { }

  public getHistory(): Observable<Transaction[]> {
    return this.rest.get('transactions/history');
  }
  public makeReceiver(ReceiverAccount: ReceiverDto): Observable<Receiver_account> {
    const tmpReceiverAccount = {...ReceiverAccount};
    return this.rest.post(`destiny_account/create_dest`, tmpReceiverAccount);
  }
  public getAccountInfo(): Observable<Account> {
    return this.rest.get('accounts/info');
  }
  public transferFounds(transaction: TransactionDto): Observable<Transaction> {
    const tmpTransaction = {...transaction};
    return this.rest.post(`accounts/transfer`, tmpTransaction);
  }
  public makeDeposit(amount: Partial<TransactionDto>): Observable<Transaction> {
    return this.rest.post('accounts/deposit', amount);
  }
  public makeWithdraw(amount: Partial<TransactionDto>): Observable<Transaction> {
    return this.rest.post('accounts/withdraw', amount);
  }
  public getBanks(): Observable<BanksDto[]> {
    return this.rest.get('banks');
  }
  public getReceiver(): Observable<Receiver_account[]> {
    return this.rest.get('destiny_account/historyAll');
  }

}
