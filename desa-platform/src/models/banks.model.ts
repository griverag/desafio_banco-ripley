

export class BanksModel {
  public id?: number;
  public bank: string;

  constructor(data?: any) {
    this.id = data.id;
    this.bank = data.name;
  }
}
