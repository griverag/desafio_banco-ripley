import { AccountType } from '../enums/account-type.enum';

export class Account {
  public id?: number;
  public acc_number_cust: number;
  public balance_cust: number;
  public rut_cust: string;
  public type: AccountType;
}
