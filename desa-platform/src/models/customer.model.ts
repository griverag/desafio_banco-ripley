export class Customer {
  public id?: number;
  public name_cust: string;
  public rut_cust: string;
  public email_cust: string;
  public phone_cust: string;
  public password_cust: string;
}
