import { Controller, Get, UsePipes, ValidationPipe, Headers } from '@nestjs/common';
import { Transaction } from 'src/models/transaction.model';
import { TransactionsService } from 'src/services/transactions/transactions.service';

@Controller('transactions')
export class TransactionsController {
  constructor(private transactionService: TransactionsService) {}

  @Get('history')
  @UsePipes(ValidationPipe)
  public getHistory(
    @Headers('rut_customer') rut_customer: string
  ): Promise<Transaction[]> {
    return this.transactionService.getHistory2(rut_customer);
  }
}
