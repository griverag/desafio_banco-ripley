import {BadRequestException, Controller, Get, NotFoundException, UsePipes, ValidationPipe} from '@nestjs/common';
import {Destiny_account} from "../../models/destiny_account.model";
import {BanksService} from "../../services/banks/banks.service";
import {BanksModel} from "../../models/banks.model";

@Controller('banks')
export class BanksController {
    constructor(private servicioBancos: BanksService) {
    }

    @Get('')
    @UsePipes(ValidationPipe)
    public getBanks(): Promise<BanksModel[]> {
        return this.servicioBancos.getBanks();
    }
}
