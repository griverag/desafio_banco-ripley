import {
  Body,
  Controller,
  Post,
  UsePipes,
  ValidationPipe,
  Headers,
  Param,
  Get,
} from '@nestjs/common';
import { AccountRS } from 'src/dto/account-rs.dto';
import { TransactionRQ } from 'src/dto/transaction-rq.dto';
import { Transaction } from 'src/models/transaction.model';
import { AccountsService } from 'src/services/accounts/accounts.service';

@Controller('accounts')
export class AccountsController {
  constructor(private accountService: AccountsService) {}

  @Get('info')
  @UsePipes(ValidationPipe)
  public getAccountInfo(
    @Headers('rut_customer') rut_customer: string,
  ): Promise<AccountRS> {
    return this.accountService.getAccount(rut_customer);
  }

  @Post('transfer')
  @UsePipes(ValidationPipe)
  public postTransfer(
    @Param('rut_dest') rut_dest,
    @Body() transaction: Transaction,
  ): Promise<Transaction> {
      return this.accountService.transferFounds(
      transaction.rut_customer,
      transaction.rut_dest,
      transaction,
    );
  }

  @Post('withdraw')
  @UsePipes(ValidationPipe)
  public withdraw(
    @Headers('rut_customer') rut_customer: string,
    @Body() transaction: Transaction,
  ): Promise<Transaction> {
    return this.accountService.withdraw(rut_customer, transaction);
  }

  @Post('deposit')
  @UsePipes(ValidationPipe)
  public loadBalance(
    @Headers('rut_customer') rut_customer: string,
    @Body() transaction: Transaction,
  ): Promise<Transaction> {
    return this.accountService.loadBalance(rut_customer, transaction);
  }
}
