import {
  Body,
  Controller,
  Get,
  Headers,
  Post,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { Destiny_account } from '../../models/destiny_account.model';
import { Destiny_accountService } from '../../services/destiny_accounts/destiny_account.service';
import { Destiny_AccountRS } from '../../dto/destiny_account-rs.dto';

@Controller('destiny_account')
export class Destiny_accountController {
  constructor(private destiny_accountService: Destiny_accountService) {}

  @Get('history_dest')
  @UsePipes(ValidationPipe)
  public getDestinatario(
    @Headers('rut_dest') rut_dest: string,
  ): Promise<Destiny_AccountRS> {
    return this.destiny_accountService.getDestinatario(rut_dest);
  }
  @Get('historyAll')
  @UsePipes(ValidationPipe)
  public getDestinatarios(): Promise<Destiny_account> {
    return this.destiny_accountService.getDestinatarios();
  }

  @Post('create_dest')
  @UsePipes(ValidationPipe)
  public makeReceiver(
    @Headers()
    @Body() destinyAccountRS: Destiny_AccountRS,
  ): Promise<Destiny_account> {
    return this.destiny_accountService.createDestinatario(destinyAccountRS);
  }
}
