import { Test, TestingModule } from '@nestjs/testing';
import { Destiny_accountController } from './destiny_account.controller';

describe('Destiny_accountController', () => {
  let controller: Destiny_accountController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [Destiny_accountController],
    }).compile();

    controller = module.get<Destiny_accountController>(
      Destiny_accountController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
