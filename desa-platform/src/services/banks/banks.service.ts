import {
    BadRequestException,
    HttpService,
    Injectable,
    MethodNotAllowedException,
    NotFoundException,
} from '@nestjs/common';
import {BanksModel} from "../../models/banks.model";

@Injectable()
export class BanksService {


    constructor(private http: HttpService) {
    }

    public async getBanks(): Promise<BanksModel[]> {
        return new Promise((resolve, rejects) => {
            this.http.get(`https://bast.dev/api/banks.php`).subscribe(response => {
                if (!response.data.banks.length) throw new NotFoundException();
                resolve(response.data.banks.map(banks => new BanksModel(banks)));
            }, error => {
                throw new BadRequestException();
            });
        });
    }
}
