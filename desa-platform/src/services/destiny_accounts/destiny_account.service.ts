import { BadRequestException, HttpService, Injectable, NotFoundException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { RequestQueryBuilder } from '@nestjsx/crud-request';
import { Destiny_AccountRS } from 'src/dto/destiny_account-rs.dto';
import { Destiny_account } from '../../models/destiny_account.model';
import { ParserService } from '../parser/parser.service';
@Injectable()
export class Destiny_accountService {
  private path: string;

  constructor(
    private configService: ConfigService,
    private parser: ParserService,
    private http: HttpService,
  ) {
      this.path = this.configService.get('DESA_DATA');
  }
  public async createDestinatario(
    destiny_account: Partial<Destiny_account>,
  ): Promise<Destiny_account> {
    return new Promise((resolve, reject) => {
      this.http
        .post(`${this.path}/destiny_account`, destiny_account)
        .subscribe(
          (response) => {
            resolve(new Destiny_account(response.data));
          },
          (err) => {
            throw new BadRequestException(err);
          },
        );
    });
  }
  public async getDestinatario(rut_dest: string): Promise<Destiny_AccountRS> {
    return new Promise<Destiny_AccountRS>((resolve, rejects) => {
      const query = RequestQueryBuilder.create()
        .setFilter({
          field: 'rut_dest',
          operator: '$eq',
          value: rut_dest,
        });
      this.http.get(`${this.path}/destiny_account?${this.parser.parse(query)}`).subscribe(
          (response) => {
            if (response.data.length) {
              resolve(new Destiny_AccountRS(response.data[0]));
            } else {
              rejects(new NotFoundException('DESTINATARIO NO EXISTENTE'));
            }
          },
          (error) => {
            rejects(new BadRequestException());
          },
        );
    });
  }
    public async getDestinatarios(): Promise<Destiny_account> {
        return new Promise((resolve, rejects) => {
            this.http.get(`${this.path}/destiny_account`).subscribe(response => {
                if (!response.data.length) throw new NotFoundException();
                resolve(response.data.map(destiny_account => new Destiny_account(destiny_account)));
            }, error => {
                throw new BadRequestException();
            });
        });
    }
  public async getDestinatariosCustomer(rut_cust: string): Promise<Destiny_account> {
    return new Promise((resolve, rejects) => {
        const query = RequestQueryBuilder.create()
            .setFilter({
                field: 'customer.rut_cust',
                operator: '$eq',
                value: rut_cust,
            });
       this.http.get(`${this.path}/destiny_account?${this.parser.parse(query)}`).subscribe(response => {
           if (!response.data.length) throw new NotFoundException();
           resolve(response.data.map(destiny_account => new Destiny_account(destiny_account)));
       }, error => {
           throw new BadRequestException();
       });
    });
  }
}
