import { Test, TestingModule } from '@nestjs/testing';
import { Destiny_accountService } from './destiny_account.service';

describe('Destiny_accountService', () => {
    let service: Destiny_accountService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [Destiny_accountService],
        }).compile();

        service = module.get<Destiny_accountService>(Destiny_accountService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
