import { BadRequestException, HttpService, Injectable, NotFoundException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { RequestQueryBuilder } from '@nestjsx/crud-request';
import { Transaction } from 'src/models/transaction.model';
import { ParserService } from '../parser/parser.service';
import {Destiny_AccountRS} from "../../dto/destiny_account-rs.dto";

@Injectable()
export class TransactionsService {
  public path: string;

  constructor(
    private http: HttpService,
    private configService: ConfigService,
    private parser: ParserService
  ) {
        this.path = this.configService.get('DESA_DATA');
  }

  public async createTransaction(transaction: Partial<Transaction>): Promise<Transaction> {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.path}/transactions`, transaction).subscribe(response => {
        resolve(new Transaction(response.data));
      }, err => {
        throw new BadRequestException(err);
      });
    });
  }

  public async getHistory(rut_customer: string): Promise<Transaction[]> {
    return new Promise<Transaction[]>((resolve, reject) => {
      const query = RequestQueryBuilder.create()
      .setFilter({ field: 'rut_customer', operator: '$eq', value: rut_customer})
      .sortBy({ field: 'createdAt', order: 'DESC'})
      this.http.get(`${this.path}/transactions?${this.parser.parse(query)}`)
        .subscribe(response => {
          if (!response.data.length) throw new NotFoundException();
          resolve(response.data.map(transaction => new Transaction(transaction)));
        }, error => {
          throw new BadRequestException();
        });
    });
  }
  public async getHistory2(rut_customer: string): Promise<Transaction[]> {
    return new Promise<Transaction[]>((resolve, rejects) => {
      const query = RequestQueryBuilder.create()
          .setFilter({
            field: 'rut_customer',
            operator: '$eq',
            value: rut_customer
          })
        .sortBy({ field: 'createdAt', order: 'DESC'})
      this.http.get(`${this.path}/transactions?${this.parser.parse(query)}`).subscribe(
          (response) => {
              if (response.data?.length) {
                   resolve(response.data.map(transaction => new Transaction(transaction)));
                  return;
            } else {
              rejects(new NotFoundException('Transactions not found'));
            }
          },
          (error) => {
            rejects(new BadRequestException());
          },
      );
    });
  }
}
