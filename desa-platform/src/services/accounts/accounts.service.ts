import {
  BadRequestException,
  HttpService,
  Injectable,
  MethodNotAllowedException,
  NotFoundException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { RequestQueryBuilder } from '@nestjsx/crud-request';
import { AccountRS } from 'src/dto/account-rs.dto';
import { PatchAccountDto } from 'src/dto/patch-account.dto';
import { TransactionRQ } from 'src/dto/transaction-rq.dto';
import { TransactionType } from 'src/enums/transaction-type.enum';
import { Transaction } from 'src/models/transaction.model';
import { ParserService } from '../parser/parser.service';
import { TransactionsService } from '../transactions/transactions.service';
import { Destiny_accountService } from '../destiny_accounts/destiny_account.service';

@Injectable()
export class AccountsService {
  private path: string;

  constructor(
    private configService: ConfigService,
    private parser: ParserService,
    private http: HttpService,
    private transactionService: TransactionsService,
    private destiny_accountService: Destiny_accountService,
  ) {
    this.path = this.configService.get('DESA_DATA');

  }

  public async transferFounds(
    rut_customer: string,
    rut_dest: string,
    transaction: TransactionRQ,
  ): Promise<Transaction> {
    // Validate sufficient balance
    const transferor = await this.getAccount(rut_customer);
    if (transferor.balance_cust <= transaction.amount)
      throw new MethodNotAllowedException('INSUFFICIENT_FUNDS');
    // Checks valid destinationAccount
    const transferee = await this.destiny_accountService.getDestinatario(
      rut_dest,
    );
    // Subtract amount of origin account
    await this.patchAccount(
      transferor.id,
      new PatchAccountDto({
        balance: transferor.balance_cust - transaction.amount,
      }),
    );
    // Create transaction

    const originTransaction = await this.transactionService.createTransaction({
      amount: transaction.amount,
      type: TransactionType.TRANSFEROUT,
      rut_dest: transferee.rut_dest,
      rut_customer: transferor.rut_cust,
    });
    /*
    // Create transaction
    await this.transactionService.createTransaction({
      amount: transaction.amount,
      type: TransactionType.TRANSFERIN,
      rut_dest: transferee.rut_dest,
      rut_customer: transferor.rut_cust,
    });
    */
    return originTransaction;
  }

  public async getAccount(rut_customer: string): Promise<AccountRS> {
    return new Promise<AccountRS>((resolve, rejects) => {
      const query = RequestQueryBuilder.create()
        .setJoin({ field: 'customer' })
        .setFilter({
          field: 'customer.rut_cust',
          operator: '$eq',
          value: rut_customer,
        });
      this.http
        .get(`${this.path}/accounts?${this.parser.parse(query)}`)
        .subscribe(
          (response) => {
            if (response.data.length) {
              resolve(new AccountRS(response.data[0]));
            } else {
              rejects(new NotFoundException('NON-EXISTENT_ACCOUNT'));
            }
          },
          (error) => {
            rejects(new BadRequestException());
          },
        );
    });
  }

  public async patchAccount(
    accountId: number,
    payload: PatchAccountDto,
  ): Promise<AccountRS> {
    return new Promise<AccountRS>((resolve, rejects) => {
      this.http.patch(`${this.path}/accounts/${accountId}`, payload).subscribe(
        (response) => {
          resolve(new AccountRS(response.data[0]));
        },
        (error) => {
          rejects(new BadRequestException());
        },
      );
    });
  }

  public async withdraw(
    rut_customer: string,
    transaction: Transaction,
  ): Promise<Transaction> {
    // Validate sufficient balance
    const account = await this.getAccount(rut_customer);
    if (account.balance_cust <= transaction.amount)
      throw new MethodNotAllowedException('INSUFFICIENT_FUNDS');

    // Substract balance
    await this.patchAccount(
      account.id,
      new PatchAccountDto({
        balance: account.balance_cust - transaction.amount,
      }),
    );

    // Create transaction
    return await this.transactionService.createTransaction({
      amount: transaction.amount,
      type: TransactionType.WITHDRAW,
      rut_customer: account.rut_cust,
      rut_dest: '',
    });
  }

  public async loadBalance(
    rut_customer: string,
    transaction: Transaction,
  ): Promise<Transaction> {
    const account = await this.getAccount(rut_customer);

    // Substract balance
    await this.patchAccount(
      account.id,
      new PatchAccountDto({
        balance: account.balance_cust + transaction.amount,
      }),
    );

    // Create transaction
    return await this.transactionService.createTransaction({
      amount: transaction.amount,
      type: TransactionType.DEPOSIT,
      rut_customer: rut_customer,
      rut_dest: '',
    });
  }
}
