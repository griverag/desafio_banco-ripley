import { HttpModule, Module } from '@nestjs/common';
import { AccountsService } from './services/accounts/accounts.service';
import { TransactionsService } from './services/transactions/transactions.service';
import { TransactionsController } from './controllers/transactions/transactions.controller';
import { AccountsController } from './controllers/accounts/accounts.controller';
import { CustomersController } from './controllers/customers/customers.controller';
import { ConfigService } from '@nestjs/config';
import { ParserService } from './services/parser/parser.service';
import { Destiny_accountService } from './services/destiny_accounts/destiny_account.service';
import { Destiny_accountController } from './controllers/destiny_account/destiny_account.controller';
import {BanksService} from "./services/banks/banks.service";
import {BanksController} from "./controllers/banks/banks.controller";

@Module({
  imports: [HttpModule],
  controllers: [
    TransactionsController,
    AccountsController,
    CustomersController,
    Destiny_accountController,
    BanksController,
  ],
  providers: [
    AccountsService,
    TransactionsService,
    ConfigService,
    ParserService,
    Destiny_accountService,
    BanksService,
  ],
})
export class AppModule {}
