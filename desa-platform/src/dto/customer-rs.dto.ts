export class CustomerRsDto {
  public id: number;
  public name_cust: string;
  public rut_cust: string;
  public email_cust: string;
  public phone_cust: string;
  public password_cust: string;

  constructor(data?: any) {
    if (data) {
      this.id = data.id;
      this.name_cust = data.name_cust;
      this.rut_cust = data.rut_cust;
      this.email_cust = data.email_cust;
      this.phone_cust = data.phone_cust;
      this.password_cust = data.password_cust;
    }
  }
}
