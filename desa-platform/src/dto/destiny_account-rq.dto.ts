import { IsNotEmpty } from "class-validator";

export class destiny_accountRqDto {
    @IsNotEmpty()
    public rut_dest: string;
}
