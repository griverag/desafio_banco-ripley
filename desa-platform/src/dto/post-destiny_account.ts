import {IsEnum, IsNotEmpty} from 'class-validator';
import { AccountType } from '../enums/account-type.enum';

export class postDestiny_account {
  @IsNotEmpty()
  public id?: number;
  @IsNotEmpty()
  public name_dest: string;
  @IsNotEmpty()
  public rut_dest: string;
  @IsNotEmpty()
  public email_dest: string;
  @IsNotEmpty()
  public phone_dest: string;
  @IsNotEmpty()
  public bank_dest: string;
  @IsNotEmpty()
  public account_type_dest: string;
  @IsNotEmpty()
  public account_number_dest: number;
  @IsNotEmpty()
  @IsEnum(AccountType)
  public type: AccountType;
}
