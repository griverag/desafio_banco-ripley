import { Test, TestingModule } from '@nestjs/testing';
import { Destiny_account_controller } from './destiny_account_controller';

describe('Destiny_accountController', () => {
  let controller: Destiny_account_controller;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [Destiny_account_controller],
    }).compile();

    controller = module.get<Destiny_account_controller>(
      Destiny_account_controller,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
