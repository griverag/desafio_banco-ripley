import { Controller } from '@nestjs/common';
import { Crud, CrudController } from '@nestjsx/crud';
import { Destiny_accountEntity } from '../entity/destiny_account.entity';
import { Destiny_accountService } from '../service/destiny_account.service';
import { CustomerEntity } from '../../customers/entity/customer.entity';

@Crud({
  model: {
    type: Destiny_accountEntity,
  },
})
@Controller('destiny_account')
export class Destiny_account_controller
  implements CrudController<Destiny_accountEntity> {
  constructor(public service: Destiny_accountService) {}
}
