import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Destiny_account_controller } from './controller/destiny_account_controller';
import { Destiny_accountEntity } from './entity/destiny_account.entity';
import { Destiny_accountService } from './service/destiny_account.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Destiny_accountEntity])],
  controllers: [Destiny_account_controller],
  providers: [Destiny_accountService]
})
export class Destiny_accountModule {}
