import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Repository } from 'typeorm';
import { Destiny_accountEntity } from '../entity/destiny_account.entity';

@Injectable()
export class Destiny_accountService extends TypeOrmCrudService<Destiny_accountEntity> {
  constructor(
    @InjectRepository(Destiny_accountEntity)
    repo: Repository<Destiny_accountEntity>,
  ) {
    super(repo);
  }
}
