import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
  BaseEntity,
  CreateDateColumn,
  UpdateDateColumn,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { AccountType } from '../../../enums/account-type.enum';
import { Destiny_account } from '../../../models/destiny_account.model';
import { CustomerEntity } from '../../customers/entity/customer.entity';

@Entity('destiny_account')
export class Destiny_accountEntity
  extends BaseEntity
  implements Destiny_account {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({
    unique: true,
  })
  @Column()
  public name_dest: string;
  @Column()
  public rut_dest: string;
  @Column()
  public email_dest: string;
  @Column()
  public phone_dest: string;
  @Column()
  public bank_dest: string;
  @Column()
  public account_number_dest: number;
  @Column()
  public type: AccountType;

  @ManyToOne(() => CustomerEntity, (customer) => customer.rut_cust)
  @JoinColumn({ name: 'customer' })
  public customer: CustomerEntity;
}
