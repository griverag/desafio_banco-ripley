import { Customer } from 'src/models/customer.model';
import { AccountEntity } from 'src/modules/accounts/entity/account.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn,
  BaseEntity,
  CreateDateColumn,
  UpdateDateColumn,
  BeforeInsert,
  BeforeUpdate,
} from 'typeorm';
import * as bcrypt from 'bcrypt';

@Entity('customer')
export class CustomerEntity extends BaseEntity implements Customer {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public name_cust: string;

  @Column({
    unique: true,
  })
  public rut_cust: string;

  @Column()
  public email_cust: string;

  @Column()
  public phone_cust: string;

  @Column()
  public password_cust: string;

  @OneToOne(() => AccountEntity)
  public account: AccountEntity;

  @BeforeInsert()
  @BeforeUpdate()
  public hashPassword(): void {
    if (this.password_cust) {
      const salt = bcrypt.genSaltSync(8);
      this.password_cust = bcrypt.hashSync(this.password_cust, salt);
    }
  }
}
