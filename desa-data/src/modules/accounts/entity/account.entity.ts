import { Account } from 'src/models/account.model';
import { CustomerEntity } from 'src/modules/customers/entity/customer.entity';
import { TransactionEntity } from 'src/modules/transactions/entity/transaction.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  BaseEntity,
  CreateDateColumn,
  UpdateDateColumn,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { AccountType } from '../../../enums/account-type.enum';

@Entity('account')
export class AccountEntity extends BaseEntity implements Account {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({
    unique: true,
  })
  public acc_number_cust: number;

  @Column({ type: 'bigint' })
  public balance_cust: number;

  @Column()
  public rut_cust: string;

  @Column()
  public type: AccountType;
  @OneToMany(() => TransactionEntity, (transaction) => transaction.account)
  public transactions: TransactionEntity[];

  @OneToOne(() => CustomerEntity, (customer) => customer.account)
  @JoinColumn({ name: 'customer' })
  public customer: CustomerEntity;
}
