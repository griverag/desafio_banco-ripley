import { TransactionType } from "src/enums/transaction-type.enum";

export class Transaction {
  readonly id?: number;
  readonly amount: number;
  readonly type: TransactionType;
  readonly rut_dest: string;
  readonly rut_customer: string;
  readonly createdAt: Date;
}
