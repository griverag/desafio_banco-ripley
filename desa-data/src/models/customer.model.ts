export class Customer {
  readonly id?: number;
  readonly name_cust: string;
  readonly rut_cust: string;
  readonly email_cust: string;
  readonly phone_cust: string;
  readonly password_cust: string;
}
