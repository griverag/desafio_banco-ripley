import { AccountType } from '../enums/account-type.enum';

export class Account {
  readonly id?: number;
  readonly acc_number_cust: number;
  readonly balance_cust: number;
  readonly rut_cust: string;
  readonly type: AccountType;
}
