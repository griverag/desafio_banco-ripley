import { AccountType } from '../enums/account-type.enum';

export class Destiny_account {
  readonly id?: number;
  readonly name_dest: string;
  readonly rut_dest: string;
  readonly email_dest: string;
  readonly phone_dest: string;
  readonly bank_dest: string;
  readonly account_number_dest: number;
  readonly type: AccountType;
}
