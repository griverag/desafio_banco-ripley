import { Module } from '@nestjs/common';
import { DatasourceModule } from './datasource/datasource.module';
import { CustomersModule } from './modules/customers/customers.module';
import { AccountsModule } from './modules/accounts/accounts.module';
import { TransactionsModule } from './modules/transactions/transactions.module';
import { Destiny_accountModule} from "./modules/destiny_account/destiny_account.module";

@Module({
  imports: [
    DatasourceModule,
    CustomersModule,
    AccountsModule,
    TransactionsModule,
      Destiny_accountModule
  ],
})
export class AppModule {}
