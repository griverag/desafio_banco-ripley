# Desafio_Banco-Ripley

El proyecto se desarrollo con las tecnologías a continuacion para cada parte de
la arquitectura de la aplicación:

* Despliegue microservicios: Google's Cloud Build y Cloud Run
* Base de datos: MySQL
* Microservicios:  NestJS + Trypescript + Express
* Despliegue frontend: firebase
* Frontend:  Angular 11 + Typescript
* La plataforma está disponible [MiPrueba BancoRipley](https://pruebabancoripley.web.app/nuevo-destinatario)


 * Piezas App.
* DESA-DATA: Es un microservicio que solo contiene lógica transaccional, se usa una libreria que expone endpoints CRUD para todas las entidades mapeadas con TypeORM los cuales son utilizados por otro microservicios.
* DESA-PLATFORM: Es un microservicio que encapsula la lógica de negocios interno de  (transferencias, transacciones , destinatarios
        * Nota: se debia realizar una capa de  autenticacion adicional pero falto tiempo xP

## Despliege 🚀 Desarrollo.
* Microservicios:
1. Instalar dependencias (yarn, o npm install).
2. Copiar la información de .env-sample hacia .env (crear archivo).
3. Levantar las aplicación con `yarn start:dev` o `npm start:dev`

* Frontend:
1. Instalar dependencias (yarn, o npm install).
3. Levantar las aplicación con `yarn start` o `npm start`
